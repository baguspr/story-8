from django.test import TestCase, Client
from django.urls import resolve
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class jvscUnitTest(TestCase):
    def test_jvsc_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_jvsc_views_index_func_used(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


    def test_jvsc_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_ftest_landing_page_contains_welcome(self):
        response = Client().get('/')
        self.assertContains(response, '<p class="title">NAME</p>', status_code=200)

class jvscFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(jvscFunctionalTest, self).setUp()
    
    def tearDown(self):
        selenium = self.selenium
        super(jvscFunctionalTest,self).tearDown

    def test_page(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')

        # accordion = selenium.find_element_by_name('title_test')
        up = selenium.find_element_by_name('up_button')
        down = selenium.find_element_by_name('down_button')

        # accordion.click()
        up.click()
        down.click()