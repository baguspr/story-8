from django.urls import path
from . import views

app_name = 'ftest'

urlpatterns = [
    path('', views.index, name='home'),
]